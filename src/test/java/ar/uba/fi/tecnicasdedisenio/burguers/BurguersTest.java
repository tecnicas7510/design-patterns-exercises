package ar.uba.fi.tecnicasdedisenio.burguers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/*
 * The creation of the burguer object is complex, the idea is 
 * to delegate it to another object that encapsulates it.
 * 
 * Implement the code needed to make the tests pass.
 */
public class BurguersTest {
    //  @Test
    // public void createSimpleMeetBurguer() {
    //     BurguerCreator burguerCreator = new BurguerCreator();
    //     burguerCreator.addMeet();

    //     Burguer burguer = burguerCreator.build();

    //     assertNotNull(burguer);
    //     assertEquals("Bread-Meet-Bread", burguer.describe());
    // }

    // @Test
    // public void createComplexMeetBurguer() {
    //     BurguerCreator burguerCreator = new BurguerCreator();
    //     burguerCreator.addMeet();
    //     burguerCreator.addLetuce();
    //     burguerCreator.addTomato();
    //     burguerCreator.addCucumber();

    //     Burguer burguer = burguerCreator.build();

    //     assertNotNull(burguer);
    //     assertEquals("Bread-Meet--Letuce--Tomato--Cucumber-Bread", burguer.describe());
    // }

    // @Test
    // public void createComplexCheekenBurguer() {
    //     BurguerCreator burguerCreator = new BurguerCreator();
    //     burguerCreator.addCheeken();
    //     burguerCreator.addLetuce();
    //     burguerCreator.addTomato();
    //     burguerCreator.addCucumber();

    //     Burguer burguer = burguerCreator.build();

    //     assertNotNull(burguer);
    //     assertEquals("Bread-Meet--Letuce--Tomato--Cucumber-Bread", burguer.describe());
    // }
}
