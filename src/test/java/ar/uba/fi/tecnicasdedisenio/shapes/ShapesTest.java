package ar.uba.fi.tecnicasdedisenio.shapes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/*
 * We have a shapes application that works with different kind of shapes. (Rectangle, Square)
 * We want to be able to extend it and allow to add border and maybe other features to the shapes.
 * 
 * Implement the code needed to make the tests pass.
 */
public class ShapesTest {
    @Test
    public void drawRectangleWithoutBorder() {
        Shape shape = new Rectangle();

        assertNotNull(shape);
        assertEquals("Rectangle", shape.draw());
    }

    @Test
    public void drawSquareWithoutBorder() {
        Shape shape = new Square();

        assertNotNull(shape);
        assertEquals("Square", shape.draw());
    }

    // @Test
    // public void drawRectangleWithBorder() {
    //     Shape shape = new BorderedShape(new Rectangle(), "Red");

    //     assertNotNull(shape);
    //     assertEquals("Red Rectangle", shape.draw());
    // }

    //  @Test
    // public void drawSquareWithBorder() {
    //     Shape shape = new BorderedShape(new Square(), "Yellow");

    //     assertNotNull(shape);
    //     assertEquals("Yellow Square", shape.draw());
    // }
}
