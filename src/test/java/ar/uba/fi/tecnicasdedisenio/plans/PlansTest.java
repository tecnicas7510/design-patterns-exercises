package ar.uba.fi.tecnicasdedisenio.plans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/*
 * We have a health care system that handles different plans.
 * For each plan the specific bill is calculated based on a specific rate 
 * and the quantity of units used for that plan.
 * 
 * We have 3 types of plans possible:
 * 1- Domestic plan. For this kind of plan the rate is 5. 
 * 2- Commercial plan. For this kind of plan the rate is 7.5. 
 * 3- Institutional plan. For this kind of plan the rate is 5.5.
 * 
 * Implement the code needed to make the tests pass.
 */
public class PlansTest {
    // @Test
    // public void calculateRateForDomesticPlan() {
    //     String planType = "Domestic";
    //     int units = 2;

    //     PlanCreator planCreator = new PlanCreator();

    //     Plan plan = planCreator.create(planType);

    //     assertNotNull(plan);
    //     assertEquals(10, plan.calculuateBill(units));
    // }

    // @Test
    // public void calculateRateForCommercialPlan() {
    //     String planType = "Commercial";
    //     int units = 4;

    //     PlanCreator planCreator = new PlanCreator();

    //     Plan plan = planCreator.create(planType);

    //     assertNotNull(plan);
    //     assertEquals(30, plan.calculuateBill(units));
    // }

    // @Test
    // public void calculateRateForInstitutionalPlan() {
    //     String planType = "Institutional";
    //     int units = 8;

    //     PlanCreator planCreator = new PlanCreator();

    //     Plan plan = planCreator.create(planType);

    //     assertNotNull(plan);
    //     assertEquals(44, plan.calculuateBill(units));
    // }
}
