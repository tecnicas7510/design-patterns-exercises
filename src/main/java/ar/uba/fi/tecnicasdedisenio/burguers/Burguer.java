package ar.uba.fi.tecnicasdedisenio.burguers;

import java.util.ArrayList;
import java.util.List;

public class Burguer {
    private List<String> ingredients = new ArrayList<>();

    Burguer(final List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public String describe() {
        StringBuilder builder = new StringBuilder();
        builder.append("Bread");
        for (int i=0; i < ingredients.size(); i++) {
            builder.append("-");
            builder.append(ingredients.get(i));
            builder.append("-");
        }
        builder.append("Bread");
        return builder.toString();
    }
}
