package ar.uba.fi.tecnicasdedisenio.shapes;

public interface Shape {
    public String draw();
}
