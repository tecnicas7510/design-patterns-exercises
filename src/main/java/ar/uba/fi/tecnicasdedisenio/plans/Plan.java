package ar.uba.fi.tecnicasdedisenio.plans;

abstract class Plan {
  protected double rate;

  public double calculateBill(int units) {
    return units * rate;
  }
}