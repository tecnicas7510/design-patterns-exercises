package ar.uba.fi.tecnicasdedisenio.tv;

public class TV {
    private static final int INC_VOLUME = 5;
    private static final int MAX_VOLUME = 100;
    private static final int MIN_VOLUME = 0;

    private static final int INC_CHANNEL = 1;
    private static final int MAX_CHANNEL = 50;
    private static final int MIN_CHANNEL = 0;

    private boolean isOn = false;
    private int channel = MIN_CHANNEL;
    private int volume = MIN_VOLUME;

    public void turnOn() {
        if (!this.isOn) {
            this.isOn = true;
        }
    }

    public void turnOff() {
        if (this.isOn) {
            this.isOn = false;
        }
    }

    public void volumeUp() {
        if (this.volume < MAX_VOLUME) {
            this.volume = this.volume + INC_VOLUME;
        }
    }

    public void volumeDown() {
        if (this.volume > MIN_VOLUME) {
            this.volume = this.volume - INC_VOLUME;
        }
    }

    public void channelUp() {
        if (this.channel < MAX_CHANNEL) {
            this.channel = this.channel + INC_CHANNEL;
        }
    }

    public void channelDown() {
        if (this.channel > MIN_CHANNEL) {
            this.channel = this.channel - INC_CHANNEL;
        }
    }

    public boolean isOn() {
        return this.isOn;
    }

    public int getChannel() {
        return this.channel;
    }

    public int getVolume() {
        return this.volume;
    }
}
